package com.plutoniumapps.t_square.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.plutoniumapps.t_square.AnnouncementViewActivity;
import com.plutoniumapps.t_square.Model.Announcement;
import com.plutoniumapps.t_square.Model.Course;
import com.plutoniumapps.t_square.R;
import com.plutoniumapps.t_square.TabActivity.ClassDetailFragment;
import com.plutoniumapps.t_square.TabActivity.SingleClassTabActivity;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;


public class AnnouncementsAdapter extends RecyclerView.Adapter<AnnouncementsAdapter.ViewHolder> implements UpdateableAdapter{

    private List<Announcement> mAnnouncements;
    private final SimpleDateFormat f = new SimpleDateFormat("MMM d, yyyy", Locale.US);
    private ClassDetailFragment mFrag;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.recycler_announcements_title) TextView mTitleText;
        @Bind(R.id.recycler_announcements_from) TextView mFromText;
        @Bind(R.id.recycler_announcements_date) TextView mDateText;
        @Bind(R.id.recycler_announcements_view) View mView;
        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public AnnouncementsAdapter(List<Announcement> courses, ClassDetailFragment frag) {
        mFrag = frag;
        mAnnouncements = courses;
    }

    @Override
    public AnnouncementsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_announcements, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Announcement a = mAnnouncements.get(position);
        holder.mTitleText.setText(a._subject);
        holder.mFromText.setText(a._author);
        if(a._date != null)
            holder.mDateText.setText(f.format(a._date));
        else
            holder.mDateText.setText("");

        holder.mView.setOnClickListener(view -> {

            Intent i = new Intent(holder.mTitleText.getContext(), AnnouncementViewActivity.class);
            i.putExtra(SingleClassTabActivity.ANNOUNCEMENT_VIEW_ANNOUNCEMENT, mAnnouncements.get(position));
            holder.mTitleText.getContext().startActivity(i);
        });
    }

    @Override
    public int getItemCount() {
        return (mAnnouncements==null ? 0 : mAnnouncements.size());
    }

    @Override
    public void updateCourse(Course c){
        mAnnouncements = c._announcements;
        mFrag.emptyView(c._announcements.size() == 0);
        notifyDataSetChanged();
    }

}
