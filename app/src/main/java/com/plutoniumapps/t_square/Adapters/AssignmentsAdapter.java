package com.plutoniumapps.t_square.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.plutoniumapps.t_square.Model.Assignment;
import com.plutoniumapps.t_square.Model.Course;
import com.plutoniumapps.t_square.R;
import com.plutoniumapps.t_square.TabActivity.ClassDetailFragment;
import com.plutoniumapps.t_square.Tsquare;
import com.plutoniumapps.t_square.ViewerActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AssignmentsAdapter extends RecyclerView.Adapter<AssignmentsAdapter.ViewHolder> implements UpdateableAdapter{

    private List<Assignment> mAssignments;
    private ClassDetailFragment mFrag;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.recycler_assignment_title) TextView mTitle;
        @Bind(R.id.recycler_assignment_date) TextView mDate;
        @Bind(R.id.recycler_assignment_status) TextView mStatus;
        @Bind(R.id.recycler_assignment_view) View mView;
        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public AssignmentsAdapter(List<Assignment> assignments, ClassDetailFragment frag) {
        mFrag = frag;
        mAssignments = assignments;
    }

    @Override
    public AssignmentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_assignments, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Assignment get = mAssignments.get(position);
        get.setTextView(holder.mTitle, holder.mDate,holder.mStatus);


    }

    @Override
    public int getItemCount() {
        return (mAssignments == null ? 0 : mAssignments.size());
    }

    @Override
    public void updateCourse(Course c){
        mAssignments = c._assignments;
        mFrag.emptyView(c._assignments.size() == 0);
        notifyDataSetChanged();
    }
}
