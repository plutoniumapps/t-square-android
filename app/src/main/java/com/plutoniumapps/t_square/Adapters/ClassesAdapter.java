package com.plutoniumapps.t_square.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.MaterialIcons;
import com.plutoniumapps.t_square.ClassListActivity;
import com.plutoniumapps.t_square.Model.Course;
import com.plutoniumapps.t_square.Model.Prefix;
import com.plutoniumapps.t_square.R;
import com.plutoniumapps.t_square.TabActivity.SingleClassTabActivity;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ClassesAdapter extends RecyclerView.Adapter<ClassesAdapter.ViewHolder> {
    private List<Course> mCourses;
    private Activity mActivity;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.recycler_list_main_text) TextView mMainText;
        @Bind(R.id.recycler_list_sub_text) TextView mSubText;
        @Bind(R.id.class_icon) ImageView mClassIcon;
        @Bind(R.id.single_class_view) View mLayout;
        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            mClassIcon.setImageDrawable(new IconDrawable(v.getContext(), MaterialIcons.md_computer)
                    .colorRes(R.color.primary).sizeDp(40));
        }
    }

    public ClassesAdapter(Activity activity, List<Course> courses) {
        mCourses = courses;
        mActivity = activity;
    }

    @Override
    public ClassesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                               .inflate(R.layout.recycler_class_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final Course c = mCourses.get(position);
        holder.mMainText.setText(c._cleanname);
        holder.mSubText.setText(c.classDescription());
        holder.mClassIcon.setImageDrawable(new IconDrawable(mActivity, Prefix.getIcon(c._prefix))
            .colorRes(R.color.primary)
            .sizeDp(40));
        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mActivity, SingleClassTabActivity.class);
                i.putExtra(ClassListActivity.CLASS_NAME_EXTRA, mCourses.get(position).className());
                i.putExtra(ClassListActivity.CLASS_CLEAN_NAME_EXTRA,mCourses.get(position)._cleanname);
                mActivity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (mCourses==null ? 0 : mCourses.size());
    }

    public void updateClassList(List<Course> list) {
        mCourses = list;
        if(mCourses!=null){
            Collections.sort(mCourses, (c1,c2) -> c1._cleanname.compareTo(c2._cleanname));
        }
        notifyDataSetChanged();
    }
}
