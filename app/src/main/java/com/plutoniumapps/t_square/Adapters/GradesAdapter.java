package com.plutoniumapps.t_square.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.plutoniumapps.t_square.Model.Course;
import com.plutoniumapps.t_square.Model.Grade;
import com.plutoniumapps.t_square.Model.GradeCategory;
import com.plutoniumapps.t_square.Model.Gradebook;
import com.plutoniumapps.t_square.R;
import com.plutoniumapps.t_square.TabActivity.ClassDetailFragment;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class GradesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements UpdateableAdapter{

    private Gradebook mGradeBook;
    private List<Object> items;
    private ClassDetailFragment mFrag;
    private final int GRADE=0;
    private final int CATEGORY=1;
    private final int OVERALL=3;

    public static class GradeHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.recycler_grades_title) TextView mTitle;
        @Bind(R.id.recycler_grades_grade) TextView mGrade;
        @Bind(R.id.recycler_grades_comment) TextView mComments;
        public GradeHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public static class CategoryHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.recycler_category_title) TextView mTitle;
        @Bind(R.id.recycler_category_grade) TextView mGrade;
        @Bind(R.id.recycler_category_weight) TextView mWeight;
        public CategoryHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
    public static class HeaderHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.recycler_grade_overall) TextView mTitle;
        public HeaderHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
    public GradesAdapter(Gradebook gb, ClassDetailFragment frag) {
        mGradeBook = gb;
        mFrag = frag;
        if (mGradeBook != null)
            items = mGradeBook.generateContinuousList();
    }

    @Override
    public int getItemViewType(int position){
        if(items.get(position) instanceof Grade){
            return GRADE;
        } else if (items.get(position) instanceof GradeCategory){
            return CATEGORY;
        } else {
            return OVERALL;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder;
        switch(viewType){
            case CATEGORY:
                View category = inflater.inflate(R.layout.recycler_view_grade_category,parent,false);
                viewHolder = new CategoryHolder(category);
                break;
            case OVERALL:
                View overall = inflater.inflate(R.layout.recycler_view_grade_header,parent,false);
                viewHolder = new HeaderHolder(overall);
                break;
            case GRADE:
            default:
                View grade = inflater.inflate(R.layout.recycler_view_grade_item,parent,false);
                viewHolder = new GradeHolder(grade);
                break;
        }
        return viewHolder;

    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch(holder.getItemViewType()){
            case CATEGORY:
                CategoryHolder ch = (CategoryHolder) holder;
                GradeCategory gc = (GradeCategory) items.get(position);
                ch.mTitle.setText(gc._title);
                ch.mGrade.setText(gc._grade);
                if(gc._weight.isEmpty()){
                    ch.mWeight.setVisibility(View.GONE);
                } else {
                    ch.mWeight.setVisibility(View.VISIBLE);
                    ch.mWeight.setText("Weight: " + gc._weight);
                }
                break;
            case OVERALL:
                HeaderHolder hh = (HeaderHolder) holder;
                hh.mTitle.setText(mGradeBook.overall_grade);
                break;
            case GRADE:
            default:
                GradeHolder vh = (GradeHolder) holder;
                Grade g = (Grade) items.get(position);
                vh.mGrade.setText(g.gradeNumber());
                vh.mTitle.setText(g.gradeName());
                if (g.hasComments()){
                    vh.mComments.setVisibility(View.VISIBLE);
                    vh.mComments.setText("Comment: " + g.getComments());
                } else {
                    vh.mComments.setVisibility(View.GONE);
                }
        }
    }


    @Override
    public int getItemCount() {
        return (items == null ? 0 : items.size());
    }

    @Override
    public void updateCourse(Course c){
        mGradeBook = c._gradebook;
        if(mGradeBook != null){
            items = mGradeBook.generateContinuousList();
        }
        mFrag.emptyView(items.size() == 0);
        notifyDataSetChanged();
    }
}
