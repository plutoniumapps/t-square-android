package com.plutoniumapps.t_square.Adapters;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.plutoniumapps.t_square.Model.Course;
import com.plutoniumapps.t_square.Model.File;
import com.plutoniumapps.t_square.Model.Folder;
import com.plutoniumapps.t_square.Model.Resource;
import com.plutoniumapps.t_square.Model.Syllabus;
import com.plutoniumapps.t_square.R;
import com.plutoniumapps.t_square.ResourceIcon;
import com.plutoniumapps.t_square.TabActivity.ClassDetailFragment;
import com.plutoniumapps.t_square.TabActivity.SingleClassTabActivity;
import com.plutoniumapps.t_square.Tsquare;
import com.plutoniumapps.t_square.ViewerActivity;

import org.acra.ACRA;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TransferQueue;

import butterknife.Bind;
import butterknife.ButterKnife;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import rx.android.schedulers.AndroidSchedulers;
import rx.exceptions.Exceptions;
import rx.schedulers.Schedulers;

public class ResourceAdapter extends RecyclerView.Adapter<ResourceAdapter.ViewHolder> implements UpdateableAdapter{

    private Folder mRoot;
    private Folder mFolder;
    private List<File> mCurrentDirectory;
    private ClassDetailFragment mFrag;
    private GridLayoutManager mLayout;

    private final String[] mKnonTypes = {"application/pdf","application/msword"};

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.recycler_syllabus_resource_image)
        ResourceIcon mIconImage;
        @Bind(R.id.recycler_syllabus_name) TextView mName;
        @Bind(R.id.recycler_syllabus_root_view) View mView;
        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);

        }
    }

    public ResourceAdapter(Folder root, ClassDetailFragment f, GridLayoutManager layout) {
        mRoot = root;
        mCurrentDirectory = null;
        mFrag = f;
        mLayout = layout;
    }

    @Override
    public ResourceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_resource, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final File s = mCurrentDirectory.get(position);

        int resImg;
        if(s instanceof Folder){
            Folder f = (Folder) s;
            resImg = R.drawable.folder_outline;
            holder.mView.setOnClickListener(view -> {
                if(f.numberOfChildren() > 0){
                    updateDirectory(f);
                } else {
                    mFrag.showLoad();

                    mFrag.mSub.add(((Tsquare) mFrag.getActivity().getApplication()).getModel().resolveFolder(f)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(folder -> {
                            mFrag.hideLoad();
                            updateDirectory(folder);
                        }, error -> {
                            Toast.makeText(mFrag.getActivity(), "Network Error",Toast.LENGTH_LONG).show();
                            mFrag.hideLoad();
                        })

                    );
                }
            });
        } else {
            Resource r = (Resource) s;
            if (r.type.equals(".pdf")) {
                resImg = R.drawable.file_pdf;
            } else if (r.type.equals(".doc") || r.type.equals(".docx")) {
                resImg = R.drawable.file_word;
            } else if (r.type.equals(".ppt") || r.type.equals(".pptx")) {
                resImg = R.drawable.file_powerpoint;
            } else if (r.type.equals(".xls") || r.type.equals(".xlsx") || r.type.equals(".xlsm") || r.type.equals(".xlsb")) {
                resImg = R.drawable.file_excel;
            } else if (r.type.equals(".png") || r.type.equals(".jpeg")) {
                resImg = R.drawable.file_image;
            } else if (r.type.equals(".back")) {
                resImg = R.drawable.arrow_top_left;
            } else {
                resImg = R.drawable.google_chrome;
            }

            holder.mView.setOnClickListener(view -> {
                if (r.type.equals(".text") || r.type.equals(".html")) {
                    //open a view activity
                    Intent i = new Intent(holder.mName.getContext(), ViewerActivity.class);
                    i.putExtra(ViewerActivity.VIEWER_NAME, r.name);
                    i.putExtra(ViewerActivity.VIEWER_URL, s.path);
                    if (s.path.equals("this"))
                        i.putExtra(ViewerActivity.VIEWR_DATA, ((Syllabus) s)._html);
                    mFrag.startActivity(i);
                    return;
                } else if (r.type.equals("www")) {
                    //open an external link, have to go through redirect chain first
                    System.out.println("URL in object: " + r.path);
                    mFrag.mSub.add(((Tsquare) mFrag.getActivity().getApplication()).getModel().downloadResource(r.path)
                        .map(response -> {
                            Request req = response.request();
                            String external_url = req.url().toString();

                            System.out.println("URL after redirects: " + external_url);
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setData(Uri.parse(external_url));
                            response.body().close();
                            return i;
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(intent -> {
                            mFrag.startActivity(intent);
                        }, error -> {
                            if (error instanceof IOException){
                                //couldn't extract link, fall back to saved path
                                Intent i = new Intent(Intent.ACTION_VIEW);
                                i.setData(Uri.parse(r.path));
                                try{
                                    mFrag.startActivity(i);
                                }catch(android.content.ActivityNotFoundException exp){
                                    Toast.makeText(mFrag.getActivity(),"Cannot open this resource",Toast.LENGTH_LONG);
                                }
                            } else {
                                Toast.makeText(mFrag.getActivity(), "Network Error", Toast.LENGTH_LONG).show();
                            }
                            error.printStackTrace();
                        }));

                } else if (r.type.equals(".back")){
                    //navigational button to go up a level
                    //shouldn't have to load as already in memory
                    updateDirectory(mFolder.parent);
                }

                else {

                    if(!hasWrite()){
                        return;
                    }

                    mFrag.showLoad();

                    //try to GET and see if we get back a content_type we recognize even tho there wasn't an type
                    mFrag.mSub.add(((Tsquare) mFrag.getActivity().getApplication()).getModel().downloadResource(r.path)
                            .map(response -> {
                                String fileName = response.header("Content-Disposition", "filename=\"empty\"");
                                fileName = fileName.substring(fileName.indexOf("filename=\"") + 10, fileName.lastIndexOf("\""));

                                String redirect_content_type = response.header("Content-Type","empty");
                                //figure out what will cause a download of unknown types

                                Response first_response = response;
                                Response next;

                                while ((next = first_response.priorResponse()) != null) {
                                    first_response = next;
                                }

                                System.out.println("frist response content tyep: " + first_response.header("Content-Type", "empty"));

                                String content_type = first_response.header("Content-Type","empty");
                                if (!content_type.equals("empty") && !content_type.contains("text/plain")) {
                                    content_type = first_response.header("Content-Type", "error");
                                } else {
                                    content_type = redirect_content_type;
                                }

                                if (content_type.endsWith("pdf"))
                                    content_type = "application/pdf";

                                Uri localUri = null;
                                Intent view_intent = new Intent();
                                view_intent.setAction(Intent.ACTION_VIEW);
                                System.out.println("filename: " + fileName + " content-type: " + content_type);
                                try {
                                    java.io.File f = new java.io.File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileName);
                                    FileOutputStream fos = new FileOutputStream(f);
                                    fos.write(response.body().bytes());
                                    fos.close();
                                    response.body().close();
                                    localUri = Uri.fromFile(f);
                                    view_intent.setDataAndType(localUri, content_type);
                                    return view_intent;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    throw Exceptions.propagate(e);
                                }
                            })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(intent -> {
                                try {
                                    mFrag.getActivity().startActivity(intent);
                                } catch (Exception e) {

                                    ACRA.getErrorReporter().putCustomData("Data(URI)",intent.getData().toString());
                                    ACRA.getErrorReporter().putCustomData("Type",intent.getType());
                                    ACRA.getErrorReporter().handleSilentException(e);

                                    if (e instanceof ActivityNotFoundException) {
                                        Toast.makeText(mFrag.getActivity(), "No viewer found for that tye of file", Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(mFrag.getActivity(), "Error Downloading Resource", Toast.LENGTH_LONG).show();
                                    }
                                    e.printStackTrace();
                                } finally {
                                    mFrag.hideLoad();
                                }
                            }, error -> {
                                ACRA.getErrorReporter().handleSilentException(error);
                                Toast.makeText(mFrag.getActivity(), "Network Error",Toast.LENGTH_LONG).show();
                                mFrag.hideLoad();
                            }));
                }
            });
        }

        holder.mIconImage.setImageResource(resImg);

        holder.mName.setText(s.name);
    }

    @Override
    public int getItemCount() {
        int size = (mCurrentDirectory==null ? 0 : mCurrentDirectory.size());
        return size;
    }

    @Override
    public void updateCourse(Course c){

        mRoot = c._root;
        mFolder = mRoot;
        mCurrentDirectory = mRoot.getChildren();

        mFrag.emptyView(mCurrentDirectory.size() == 0);
        notifyDataSetChanged();
    }

    private void updateDirectory(Folder f){
        mFolder = f;
        mCurrentDirectory = f.getChildren();

        if(mFolder.parent != null && !mFolder.name.equals("Root"))
            insertBackItem();

        mFrag.emptyView(mCurrentDirectory.size() == 0);
        notifyDataSetChanged();
    }

    private void insertBackItem(){
        if(mCurrentDirectory.size()>0 && !mCurrentDirectory.get(0).path.equals("back"))
            mCurrentDirectory.add(0,new Resource("Back","back",".back"));
    }

    private boolean hasWrite(){
        // Here, thisActivity is the current activity
        if (ActivityCompat.checkSelfPermission(mFrag.getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                    mFrag.getActivity(),
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );

            return false;

        }
        return true;
    }
}