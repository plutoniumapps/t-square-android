package com.plutoniumapps.t_square.Adapters;

import com.plutoniumapps.t_square.Model.Course;

/**
 * Created by cj on 2/27/16.
 */
public interface UpdateableAdapter {
    public void updateCourse(Course c);
}
