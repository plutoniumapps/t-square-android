package com.plutoniumapps.t_square;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import com.plutoniumapps.t_square.Model.Announcement;
import com.plutoniumapps.t_square.TabActivity.SingleClassTabActivity;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class AnnouncementViewActivity extends AppCompatActivity {

    private CompositeSubscription mSub;
    private Announcement mAnnouncement;

    private final SimpleDateFormat mDateFormat = new SimpleDateFormat("MMM d, yyyy h:mm a", Locale.US);

    @Bind(R.id.toolbar) Toolbar mToolBar;
    @Bind(R.id.announcement_title) TextView mTitle;
    @Bind(R.id.announcement_author) TextView mAuthor;
    @Bind(R.id.announcement_date) TextView mDate;
    @Bind(R.id.announcement_body) TextView mBody;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement_view);
        ButterKnife.bind(this);

        mAnnouncement = getIntent().getParcelableExtra(SingleClassTabActivity.ANNOUNCEMENT_VIEW_ANNOUNCEMENT);
        if(mAnnouncement == null)
            finish();
        mToolBar.setTitle("");
        
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBody.setMovementMethod(LinkMovementMethod.getInstance());


    }

    @Override
    public void onResume(){
        super.onResume();
        mSub = new CompositeSubscription();
        Tsquare t = (Tsquare) getApplication();

        final Dialog pd = new Dialog(this, R.style.AppTheme_Dark_Dialog);
        pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pd.setContentView(R.layout.dialog_progress);
        pd.setCancelable(false);
        pd.show();

        mSub.add(t.getModel().resolveAnnouncement(mAnnouncement)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(a -> {
                    mTitle.setText(a._subject);
                    mAuthor.setText(a._author);
                    mDate.setText(mDateFormat.format(a._date));
                    mBody.setText(Html.fromHtml(a._message));
                    pd.dismiss();
                }, error -> {
                    error.printStackTrace();
                    Toast.makeText(this,"Network Error",Toast.LENGTH_SHORT).show();
                    pd.dismiss();
                }));
    }

    @Override
    public void onPause(){
        super.onPause();
        if(mSub != null)
            mSub.unsubscribe();
        mSub = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
