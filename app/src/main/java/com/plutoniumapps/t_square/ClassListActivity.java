package com.plutoniumapps.t_square;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.plutoniumapps.t_square.Adapters.ClassesAdapter;
import com.plutoniumapps.t_square.Model.InvalidCredentialsException;
import com.securepreferences.SecurePreferences;

import org.acra.ACRA;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class ClassListActivity extends AppCompatActivity {

    @Bind(R.id.classes_recycler_view) RecyclerView mRecyclerView;
    @Bind(R.id.classes_swipe_view) SwipeRefreshLayout mSwipeLayout;

    private ClassesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManger;
    private CompositeSubscription sub = new CompositeSubscription();
    private Dialog mProgressDialog;
    private AlertDialog mLogoutDialog;

    public static String CLASS_NAME_EXTRA = "com.plutoniumapps.t_square.CLASS_NAME_EXTRA";
    public static String CLASS_CLEAN_NAME_EXTRA = "com.plutoniumapps.t_square.CLASS_CLEAN_NAME_EXTRA";

    //pass true to get a cached response
    private void subscribeAndLoad(boolean cached, boolean dialog){
        if(dialog) mProgressDialog.show();
        sub.unsubscribe();
        sub = new CompositeSubscription();
        Tsquare t = (Tsquare)getApplication();
        sub.add(t.getModel().courseList(cached).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success -> {
                    mAdapter.updateClassList(success);
                    dismissProgressDialog();
                    resetSwipeView();
                }, error -> {
                    dismissProgressDialog();
                    resetSwipeView();
                    if (error instanceof InvalidCredentialsException) {
                        //bad creds, show login activity
                        error.printStackTrace();
                        Intent i = new Intent(ClassListActivity.this, LoginActivity.class);
                        startActivity(i);
                    } else if (error instanceof SocketTimeoutException || error instanceof UnknownHostException){
                        Toast.makeText(this, "Connection timed out", Toast.LENGTH_LONG).show();
                    } else {
                        ACRA.getErrorReporter().handleSilentException(error);
                        error.printStackTrace();
                        Toast.makeText(this, "Network Error", Toast.LENGTH_LONG).show();
                    }
                }));
    }

    private void logout(){
        SharedPreferences pfs = new SecurePreferences(this);
        SharedPreferences.Editor editor = pfs.edit();
        editor.remove(Tsquare.KEY_USERNAME);
        editor.remove(Tsquare.KEY_PASSWORD);
        editor.commit();


        unsubscribe();

        mAdapter.updateClassList(null);
        ((Tsquare)getApplication()).getModel().logout();
        startActivity(new Intent(this, LoginActivity.class));
    }

    private void dismissProgressDialog(){
        if(mProgressDialog != null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }
    private void dismissLogoutDialog(){
        if(mLogoutDialog!=null && mLogoutDialog.isShowing())
            mLogoutDialog.dismiss();
    }

    private void resetSwipeView(){
        if(mSwipeLayout != null && mSwipeLayout.isRefreshing()){
            mSwipeLayout.setRefreshing(false);
        }
    }

    private void dismissAllDialogs() {
        dismissProgressDialog();
        dismissLogoutDialog();
        resetSwipeView();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_list);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this));

        mLayoutManger = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManger);
        mAdapter = new ClassesAdapter(this, null);
        mRecyclerView.setAdapter(mAdapter);

        mSwipeLayout.setOnRefreshListener(() -> subscribeAndLoad(false, false));

        mProgressDialog = new Dialog(this, R.style.AppTheme_Dark_Dialog);
        mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mProgressDialog.setContentView(R.layout.dialog_progress);
        mProgressDialog.setCancelable(false);


        mLogoutDialog = new AlertDialog.Builder(this)
                .setTitle("Logout?")
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", (d,w) -> logout())
                .setNegativeButton("No", (d,w) -> mLogoutDialog.dismiss())
                .create();
    }

    @Override
    public void onStart(){
        super.onStart();

        sub = new CompositeSubscription();
        subscribeAndLoad(true,true);
    }

    //RxJava lifecycle management
    @Override
    public void onResume(){
        super.onResume();
        sub = new CompositeSubscription();
    }
    @Override
    public void onPause(){
        super.onPause();
        unsubscribe();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_classlist, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_logout){
            if(mLogoutDialog!=null)
                mLogoutDialog.show();
        }

        return super.onOptionsItemSelected(item);
    }


    private void unsubscribe(){
        if (sub != null)
            sub.unsubscribe();
        sub = null;
        dismissAllDialogs();
    }

}
