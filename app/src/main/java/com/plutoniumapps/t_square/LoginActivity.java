package com.plutoniumapps.t_square;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.plutoniumapps.t_square.Model.InvalidCredentialsException;
import com.securepreferences.SecurePreferences;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.btn_login) AppCompatButton login_button;
    @Bind(R.id.input_account) EditText account_name;
    @Bind(R.id.input_password) EditText password;
    private CompositeSubscription sub = new CompositeSubscription();

    @OnClick(R.id.btn_login)
    public void loginClick(){
        //Disable click until request is back
        login_button.setEnabled(false);

        String name = account_name.getText().toString();
        String pass = password.getText().toString();

        if (name.endsWith("@gatech.edu")){
            name = name.substring(0,name.lastIndexOf("@gatech.edu"));
        }

        if (validate(name, pass)) {
            if (sub == null)
                return;
            final Dialog pd = new Dialog(this, R.style.AppTheme_Dark_Dialog);
            pd.requestWindowFeature(Window.FEATURE_NO_TITLE);
            pd.setContentView(R.layout.dialog_progress);
            pd.setCancelable(false);
            TextView tv = (TextView) pd.findViewById(R.id.loading_text);
            tv.setText("Authenticating ...");
            pd.show();

            Tsquare t = (Tsquare) getApplication();
            sub.add(t.getModel().verifyLogin(name, pass).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(success -> {
                        pd.dismiss();
                        onLoginSuccess();
                    }, error -> {
                        if (error instanceof InvalidCredentialsException){
                            error.printStackTrace();
                            Toast.makeText(this, "Username/password incorrect", Toast.LENGTH_LONG).show();
                        } else {
                            error.printStackTrace();
                            System.out.println(error.getClass().getName());
                            Toast.makeText(this, "Network Error", Toast.LENGTH_LONG).show();
                        }
                        login_button.setEnabled(true);
                        pd.dismiss();

                    }));
        } else {
            login_button.setEnabled(true);
        }
    }

    private void onLoginSuccess(){
        saveAccount();
        login_button.setEnabled(true);
        ((InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(password.getWindowToken(), 0);
        finish();
    }
    private void saveAccount(){
        String un = account_name.getText().toString();
        String p = password.getText().toString();

        SharedPreferences pfs = new SecurePreferences(this);
        SharedPreferences.Editor editor = pfs.edit();
        editor.putString(Tsquare.KEY_USERNAME, un);
        editor.putString(Tsquare.KEY_PASSWORD, p);
        editor.apply();
    }

    private boolean validate(String n, String p){
        boolean valid = true;

        if(n.isEmpty()){
            account_name.setError("enter a valid account name");
            valid = false;
        } else {
            account_name.setError(null);
        }

        if(p.isEmpty() || p.length() < 3){
            password.setError("enter a valid password");
            valid = false;
        } else{
            password.setError(null);
        }

        return valid;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        moveTaskToBack(true);
    }

    @Override
    public void onResume(){
        super.onResume();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sub = new CompositeSubscription();

    }

    @Override
    public void onPause(){
        super.onPause();
        if(sub!=null)
            sub.unsubscribe();
        sub = null;
    }

}
