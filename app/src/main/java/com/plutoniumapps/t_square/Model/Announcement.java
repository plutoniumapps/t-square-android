package com.plutoniumapps.t_square.Model;


import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.util.Date;

public class Announcement implements Parcelable{
    public Date _date;
    public String _subject;
    public String _author;
    public String _message;
    public String _messageurl;

    public Announcement(String d, String n, String a, String u){
        setDate(d);
        _subject = n;
        _author = a;
        _messageurl = u;
    }

    private void setDate(String d){
        DateFormat f = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
        try{
            _date = f.parse(d);
        } catch (Exception e){
            _date = null;
        }
    }

    public void setMessage(String message){
        _message = message;
    }

    public String toString(){
        return "Date: " + (_date == null ? "Null" : _date.toString()) + " Subject: " + _subject +
                " Author: " + _author + " Message URL: " + _messageurl + "\n";
    }

    //Only silly android paradigms lie below, nothing of use
    @Override
    public void writeToParcel(Parcel out,int flags){
        out.writeLong(_date.getTime());
        out.writeString(_subject);
        out.writeString(_author);
        out.writeString(_message);
        out.writeString(_messageurl);
    }
    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Announcement> CREATOR =
            new Parcelable.Creator<Announcement>() {
                @Override
                public Announcement createFromParcel(Parcel in) {
                    return new Announcement(in);
                }

                @Override
                public Announcement[] newArray(int size) {
                    return new Announcement[size];
                }
            };
    private Announcement(Parcel in){
        _date = new Date(in.readLong());
        _subject = in.readString();
        _author = in.readString();
        _message = in.readString();
        _messageurl = in.readString();
    }

}
