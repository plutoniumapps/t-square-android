package com.plutoniumapps.t_square.Model;


import android.view.View;
import android.widget.TextView;

import com.plutoniumapps.t_square.R;

import org.acra.ACRA;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Assignment {
    public String _name;
    String _unknown;
    String _detailUrl;
    public String _bodyHtml;
    int _status;
    Date _assigned;
    Date _due;

    public final int NOT_STARTED = 0;
    public final int SUBMITTED = 1;
    public final int RETURNED = 2;
    public final int RESUBMITTED=3;
    public final int OPEN = 4;
    public final int NO_SUBMISSION = 5;
    public final int CLOSED = 6;
    public final int DUE = 7;
    public final int MISC = 8;

    private final long mHours = 3600000l;
    private final long mDay = 86400000l;

    public Assignment(String name, String unknown, String status, String assigned, String due, String url){

        if(status.equals("Not Started"))
            _status = NOT_STARTED;
        else if (status.startsWith("Submitted"))
            _status = SUBMITTED;
        else if (status.startsWith("Returned"))
            _status = RETURNED;
        else if (status.toLowerCase().startsWith("re-submitted"))
            _status = RESUBMITTED;
        else if (status.toLowerCase().equals("open"))
            _status = OPEN;
        else if (status.toLowerCase().equals("no submission"))
            _status = NO_SUBMISSION;
        else if (status.toLowerCase().equals("closed"))
            _status = CLOSED;
        else if (status.toLowerCase().equals("due"))
            _status = DUE;
        else {
            _status = MISC;
            ACRA.getErrorReporter().handleSilentException(new IllegalStateException("Unknown Assignment Status: " + status));
        }

        _name = name;
        _unknown = unknown;

        DateFormat f = DateFormat.getDateTimeInstance(DateFormat.MEDIUM,DateFormat.SHORT, Locale.US);
        try{
            _assigned = f.parse(assigned);
        }catch(Exception e){
            ACRA.getErrorReporter().putCustomData("Date String",assigned);
            ACRA.getErrorReporter().handleSilentException(e);
            e.printStackTrace();
            _assigned = null;
        }
        try {
            _due = f.parse(due);
        } catch (Exception e) {
            ACRA.getErrorReporter().putCustomData("Date String",assigned);
            ACRA.getErrorReporter().handleSilentException(e);
            e.printStackTrace();
            _due = null;
        }

        _detailUrl = url;
    }

    public void setBodyHtml(String h){
        if(h != null && !h.isEmpty()){
            _bodyHtml = h;
        }
    }

    public String toString(){
        return "Name: " + _name + " Unknown: " + _unknown + " Status: " + _status + "Assigned: " + _assigned.toString() + " Due: " + _due.toString();
    }

    public void setTextView(TextView title, TextView sub, TextView status){
        SimpleDateFormat f = new SimpleDateFormat("EEE, MMM d", Locale.US);
        title.setText(getTitle());
        if(_due != null)
            sub.setText(f.format(_due));

        if(_status == SUBMITTED) {
            status.setVisibility(View.VISIBLE);
            status.setBackgroundResource(R.color.completed);
            status.setText("SUBMITTED");
        } else if (_status == RESUBMITTED){
            status.setVisibility(View.VISIBLE);
            status.setBackgroundResource(R.color.completed);
            status.setText("RE-SUBMITTED");
        } else if (_status == RETURNED) {
            status.setVisibility(View.VISIBLE);
            status.setBackgroundResource(R.color.completed);
            status.setText("RETURNED");
        } else if (_status == OPEN){
            status.setVisibility(View.VISIBLE);
            status.setBackgroundResource(R.color.completed);
            status.setText("OPEN");
        } else {
            if(pastDue()){
                //nothing for now

                status.setVisibility(View.INVISIBLE);
            } else if (dueSoon()) {
                status.setVisibility(View.VISIBLE);
                status.setText("DUE TODAY");
                status.setBackgroundResource(R.color.primary);
            } else {
                status.setVisibility(View.INVISIBLE);
            }
        }
    }

    public String getTitle(){
        return _name;
    }

    public boolean dueSoon(){
        if(_due == null)
            return false;
        return ( (int)((System.currentTimeMillis() - _due.getTime())/mDay) == 0);
    }
    public boolean pastDue(){
        if (_due == null)
            return false;
        return (_due.getTime() < System.currentTimeMillis());
    }
    public boolean completed() {
        return _status == SUBMITTED || _status == RETURNED;
    }
}
