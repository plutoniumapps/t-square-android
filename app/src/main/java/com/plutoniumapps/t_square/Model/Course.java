package com.plutoniumapps.t_square.Model;

import java.util.List;

public class Course {

    public String _name;
    public String _englishname;
    public String _prefix;
    public String _cleanname;

    public List<Announcement> _announcements;
    public List<Assignment> _assignments;
    public Gradebook _gradebook;
    public Folder _root;

    Course(String name){
        _name = name;
        _englishname = "";
        _prefix = parsePrefix(name);
        createCleanName(name);
    }

    public void setAnnouncements(List<Announcement> list){
        _announcements = list;
    }
    public void setAssignments(List<Assignment> list){
        _assignments = list;
    }
    public void setGradebook(Gradebook g){
        _gradebook = g;
    }
    public void setEnglishName(String s) { _englishname = s;}
    public void setResourceRoot(Folder r, List<Syllabus> s) {
        _root = r;
        _root.setSyllabus(s);
    }

    public String toString() {

        StringBuffer s = new StringBuffer();

        s.append("Course: " + _name + " English Name: " + _englishname + " With overall grade: " + _gradebook.overall_grade + "\n");

        s.append("Announcements: \n");
        for(Announcement a : _announcements){
            s.append(a.toString());
        }

        s.append("\n Assignments: \n");
        for (Assignment a : _assignments){
            s.append("\t" + a.toString() + "\n");
        }

        s.append("Grades: \n");
        s.append(_gradebook.toString());

        s.append("Resources: \n");
        s.append(_root.toString() + "\n");

        return s.toString();
    }

    public String className(){
        return _name;
    }

    public String classDescription(){
        return _englishname;
    }

    public String parsePrefix(String name){
        String[] parts = name.split("-");
        if(parts.length < 2){
            return "";
        }
        return parts[0];
    }

    private void createCleanName(String name){
        int space_index = name.indexOf(" ");
        if(space_index > 0)
            name = name.substring(0, space_index);
        name = name.replace("-", " ").replace(",", " ");
        _cleanname = name;
    }

}
