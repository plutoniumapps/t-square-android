package com.plutoniumapps.t_square.Model;

/**
 * Created by cj on 3/27/16.
 */
public class File {
    public String name;
    public String path;

    public File(String n, String p){
        name = n;
        path = p;
    }

    public String toString(){
        return "Name: " + name + " Path: " + path + "\n";
    }
}
