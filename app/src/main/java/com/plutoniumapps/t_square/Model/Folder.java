package com.plutoniumapps.t_square.Model;

import java.util.ArrayList;
import java.util.List;

public class Folder extends File {
    String collectionID;
    String navRoot;
    public Folder parent;
    private List<File> children;

    //only if root folder
    private List<Syllabus> syllabus;

    public Folder(String name, String path, String colID, String nav, Folder p){
        super(name, path);
        collectionID = colID;
        parent = p;
        navRoot = nav;
        children = new ArrayList<>();
    }

    public Folder(){
        super("Empty", "");
        collectionID = "";
        parent = null;
        children = new ArrayList<>();
    }

    public List<File> getChildren(){
        if (syllabus != null && !syllabus.isEmpty()){
            List<File> merge = new ArrayList<File>(syllabus);
            merge.addAll(children);
            return merge;
        } else {
            return children;
        }
    }
    public void append(File r){
        children.add(r);
    }

    public void clear(){
        children.clear();
    }

    public int numberOfChildren(){
        return children.size();
    }
    public String getUrl(){
        String param = String.format("?source=0&criteria=title&sakai_action=doNavigate&collectionId=%s&navRoot=%s",collectionID,navRoot);
        return path + param;
    }

    public void setSyllabus(List<Syllabus> s){ syllabus = s; }


    public String toString(){
        StringBuffer b = new StringBuffer();
        b.append("Folder: " + name + "\n");

        if (syllabus != null && !syllabus.isEmpty()){
            b.append("Syllabus Files: \n");
            for(Syllabus s : syllabus){
                b.append("\t " + s.toString() + "\n");
            }
        }
        if (!children.isEmpty()){
            b.append("Files: \n");
            for (File c : children){
                b.append("\t" + c.toString() + "\n");
            }
        }
        return b.toString();

    }
}
