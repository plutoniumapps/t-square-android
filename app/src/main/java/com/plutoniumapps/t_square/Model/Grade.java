package com.plutoniumapps.t_square.Model;

import java.text.DateFormat;
import java.util.Date;


public class Grade {
    String _name;

    //Can be null if no date was found
    Date _due;

    String _grade;
    String _comments;

    Grade(String name, String due, String grade, String comments) {
        _name = name;
        _grade = grade.replaceAll("\\(|\\)","");
        _comments = comments.equals("") ? null : comments;

        DateFormat f = DateFormat.getDateInstance(DateFormat.MEDIUM);
        try {
            _due = f.parse(due);
        } catch (Exception e){
            _due = null;
        }

    }

    public String gradeName(){
        return _name;
    }

    public String gradeNumber(){
        return _grade;
    }

    public boolean hasComments(){
        return (_comments != null && !_comments.isEmpty());
    }

    public String getComments(){
        if(_comments != null){
            return _comments;
        }
        return "";
    }

    public String toString() {
        return "name: " + _name +  " due: "+ (_due == null ? "-" : _due.toString()) + " grade: " + _grade + " comments: " + (_comments == null ? " null " : _comments);
    }

}
