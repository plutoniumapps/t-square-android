package com.plutoniumapps.t_square.Model;

public class GradeCategory {
    public String _title;
    public String _grade;
    public String _weight;

    public static final GradeCategory anon = new GradeCategory("","","");

    public GradeCategory(String title, String grade, String weight){
        _title = title;
        _grade = grade.replaceAll("\\(|\\)","");
        _weight = weight;
    }

    public boolean anonCategory(){
        return this.equals(anon);
    }

    public String toString(){
        return (anonCategory() ? "Anonymous category" : "Title : " + _title + "\t Grade: " + _grade + " \t Weight: " + _weight);
    }

    public boolean equals(Object obj){
        if(!(obj instanceof GradeCategory))
            return false;
        GradeCategory other = (GradeCategory) obj;
        return _title.equals(other._title) && _grade.equals(other._grade) && _weight.equals(other._weight);
    }

    public int hashCode(){
        return _title.hashCode() % Integer.MAX_VALUE + _grade.hashCode()%Integer.MAX_VALUE + _weight.hashCode()%Integer.MAX_VALUE;
    }

}
