package com.plutoniumapps.t_square.Model;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Gradebook {
    public String overall_grade;
    public Map<GradeCategory,List<Grade>> grades;

    public Gradebook(String overall_grade, LinkedHashMap<GradeCategory,List<Grade>> grades){
        this.overall_grade = overall_grade.replaceAll("\\(|\\)","");
        this.grades = grades;
    }
    public Gradebook() {
        grades = new LinkedHashMap<>();
        overall_grade = "";
    }

    public String toString(){
        StringBuffer s = new StringBuffer();

        for (GradeCategory gc : grades.keySet()){
            s.append(gc.toString()+"\n");
            for(Grade g : grades.get(gc)){
                s.append("\t"+g.toString() + "\n");
            }
        }
        return "Gradebook size: " + grades.size() + " \tOverall Grade: " + overall_grade + "\n" + s.toString();
    }

    public int numberOfNonAnonCategoies(){
        return (grades.containsKey(GradeCategory.anon) ? grades.keySet().size()-1:  grades.keySet().size());
    }

    public List<Object> generateContinuousList(){
        List<Object> list = new ArrayList<>();
        if(overall_grade != null && !overall_grade.isEmpty())
            list.add(overall_grade);
        for (GradeCategory gc: grades.keySet()){
            if(!gc.anonCategory())
                list.add(gc);
            for(Grade g : grades.get(gc)){
                list.add(g);
            }
        }
        return list;
    }
}
