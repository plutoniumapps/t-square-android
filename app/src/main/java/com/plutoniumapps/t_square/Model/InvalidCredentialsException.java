package com.plutoniumapps.t_square.Model;

public class InvalidCredentialsException extends IllegalArgumentException {
    InvalidCredentialsException(String s){
        super(s);
    }
}
