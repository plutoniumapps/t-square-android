package com.plutoniumapps.t_square.Model;
import com.joanzapata.iconify.fonts.IoniconsIcons;
import com.joanzapata.iconify.fonts.MaterialCommunityIcons;

public class Prefix {

    public static final String computer = MaterialCommunityIcons.mdi_code_tags.key();
    public static final String accounting = MaterialCommunityIcons.mdi_calculator.key();
    public static final String architecture = MaterialCommunityIcons.mdi_bank.key();
    public static final String econ = MaterialCommunityIcons.mdi_chart_line.key();
    public static final String engineering = MaterialCommunityIcons.mdi_settings.key();
    public static final String humanities = MaterialCommunityIcons.mdi_school.key();
    public static final String language = MaterialCommunityIcons.mdi_book_open.key();
    public static final String math = MaterialCommunityIcons.mdi_function.key();
    public static final String music = MaterialCommunityIcons.mdi_music_note.key();
    public static final String science = MaterialCommunityIcons.mdi_beaker_outline.key();
    public static final String space = MaterialCommunityIcons.mdi_rocket.key();
    public static final String world = IoniconsIcons.ion_android_globe.key();



    public static String getIcon(String prefix){

        if (prefix.equals("CX") || prefix.equals("CSE") || prefix.equals("CS")){
            return computer;
        } else if (prefix.equals("ACCT")){
            return accounting;
        } else if (prefix.equals("AE") || prefix.equals("AS")){
            return space;
        } else if (prefix.equals("APPH") || prefix.equals("BIOL") || prefix.equals("BMEJ") || prefix.equals("BMED") || prefix.equals("BMEM") || prefix.equals("CHBE") ||
            prefix.equals("CHEM") || prefix.equals("BIOL") || prefix.equals("HS") || prefix.equals("MP") || prefix.equals("MSL") || prefix.equals("NS") ||
            prefix.equals("NRE") || prefix.equals("PHYS") || prefix.equals("PSYC")){
            return science;
        } else if (prefix.equals("ASE") || prefix.equals("COE") || prefix.equals("ECEP") || prefix.equals("ECE") || prefix.equals("ISYE") || prefix.equals("ID") ||
            prefix.equals("MSE") || prefix.equals("ME") || prefix.equals("PTFE")){
            return engineering;
        } else if (prefix.equals("ARBC") || prefix.equals("CHIN") || prefix.equals("ENGL") || prefix.equals("FREN") || prefix.equals("GRMN") || prefix.equals("JAPN") ||
            prefix.equals("KOR") || prefix.equals("LING") || prefix.equals("LMC") || prefix.equals("PERS") || prefix.equals("RUSS") || prefix.equals("SPAN")){
            return language;
        } else if (prefix.equals("ARCH") || prefix.equals("BC") || prefix.equals("CP") || prefix.equals("CEE") || prefix.equals("COA")){
            return architecture;
        } else if (prefix.equals("CETL") || prefix.equals("COOP") || prefix.equals("UCGA") || prefix.equals("EAS") || prefix.equals("FS") || prefix.equals("GTL") ||
            prefix.equals("HIST") || prefix.equals("HTS") || prefix.equals("IPCO") || prefix.equals("IPIN") || prefix.equals("IPFS") || prefix.equals("INTA") ||
            prefix.equals("IL") || prefix.equals("INTN") || prefix.equals("IMBA") || prefix.equals("DOPP")){
            return world;
        } else if (prefix.equals("ECON")){
            return econ;
        } else if (prefix.equals("LS") || prefix.equals("MGT") || prefix.equals("MOT") || prefix.equals("PHIL") || prefix.equals("POL") || prefix.equals("PUBP") ||
            prefix.equals("PUBJ") || prefix.equals("SOC")){
            return humanities;
        } else if (prefix.equals("MATH")){
            return math;
        } else if (prefix.equals("MUSI") || prefix.equals("CSE") || prefix.equals("CS")){
            return music;
        }
        return humanities;
    }
}
