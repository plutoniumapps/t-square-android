package com.plutoniumapps.t_square.Model;

public class Syllabus extends Resource{

    //Empty unless the syllabus is just plain html directly on the syllabus page
    public String _html;


    public Syllabus(String name, String path, String type){
        super(name, path, type);

    }

    public String toString(){
        return "Resource Name: " + name  + " Resource Type: " + type + " Resource URL: " + path;
    }

    public void setHtml(String html){
        _html = html;
    }

}
