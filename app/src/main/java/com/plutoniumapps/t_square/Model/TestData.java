package com.plutoniumapps.t_square.Model;

public class TestData {
    public static String pda = "<div class=\"portletpda\">\n" +
            "    <!-- include site html if exist-->\n" +
            "    <ul id=\"pda-portlet-menu\">\n" +
            "        <li class=\"instanceNote\"><a href=\n" +
            "        \"https://t-square.gatech.edu/portal/pda/\" title=\n" +
            "        \"Back to sites list\"><span class=\"skip\">Sites</span></a></li>\n" +
            "        <li class=\"sitesLink\"><span><a href=\n" +
            "        \"https://t-square.gatech.edu/portal/pda/\" title=\n" +
            "        \"Back to sites list\">Sites</a></span></li>\n" +
            "        <!-- WURFL Small Display -->\n" +
            "        <li class=\"logoutLink\"><span><a href=\n" +
            "        \"https://t-square.gatech.edu/portal/pda/?force.logout=yes\">Log\n" +
            "        Out</a></span></li><!-- End WURFL Small Display -->\n" +
            "    </ul><!--end of menu bar -->\n" +
            "    <ul id=\"pda-portlet-site-menu\">\n" +
            "        <li><span><a href=\n" +
            "        \"https://t-square.gatech.edu/portal/pda/%7Eeb0c903240dee0a5bbf6453e323db24a\"\n" +
            "        title=\"My Workspace\">My Workspace</a></span></li>\n" +
            "        <li><span><a href=\n" +
            "        \"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426\"\n" +
            "        title=\"LMC-3432-JDA,JDB\">LMC-3432-JDA,JDB</a></span></li>\n" +
            "        <li><span><a href=\n" +
            "        \"https://t-square.gatech.edu/portal/pda/ad49c1b3-1383-40f4-9fcf-82cc745e1558\"\n" +
            "        title=\"Team 12 - Destination on\">Team 12 - Destination\n" +
            "        on</a></span></li>\n" +
            "    </ul>\n" +
            "    <div id=\"pda-footer\">\n" +
            "        <a class=\"switch-link\" href=\n" +
            "        \"https://t-square.gatech.edu/portal/pda/?force.classic=yes\">Switch\n" +
            "        to Full View</a>\n" +
            "    </div>\n" +
            "</div>";

    public static String course = "<div class=\"portletpda\">\n" +
            "\t<!-- include site html if exist-->\n" +
            "\t  <ul id=\"pda-portlet-menu\">\n" +
            "\t\t<li class=\"instanceNote\"><a href=\"https://t-square.gatech.edu/portal/pda/\" title=\"Back to sites list\"><span class=\"skip\">Sites</span></a></li>\n" +
            "  \t<li class=\"sitesLink\"><span><a href=\"https://t-square.gatech.edu/portal/pda/\" title=\"Back to sites list\">Sites</a></span></li>\n" +
            "       <li class=\"currentSiteLink\"><span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426\" title=\"LMC-3432-JDA,JDB\">LMC-3432-JDA,JDB</a></span></li>\n" +
            "    <!-- WURFL Small Display -->\n" +
            "      <li class=\"logoutLink\"><span><a href=\"https://t-square.gatech.edu/portal/pda/?force.logout=yes\">Log Out</a></span></li>\n" +
            "    <!-- End WURFL Small Display -->\n" +
            "</ul> <!--end of menu bar -->\n" +
            "      <ul id=\"pda-portlet-page-menu\"> \n" +
            "                  <li class=\"icon-sakai-iframe-site-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/9321ce32-2255-46cf-aa15-58ad3b0f5d75\" title=\"LMC 3432-JDA &amp; JDB\"  class=\"icon-sakai-iframe-site\">LMC 3432-JDA &amp; JDB</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-synoptic-announcement-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/74a283a6-8fd4-4bd7-9de3-522ea06de348\" title=\"Recent Announcements\"  class=\"icon-sakai-synoptic-announcement\">Recent Announcements</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-synoptic-messagecenter-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/e8d1c278-1128-4576-8c87-d9329a034522\" title=\"Message Center Notifications\"  class=\"icon-sakai-synoptic-messagecenter\">Message Center Notifications</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-syllabus-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/de562a81-0d35-4160-8e70-e4662e115f08\" title=\"Syllabus\"  class=\"icon-sakai-syllabus\">Syllabus</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-announcements-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/72817783-794f-4dca-9c5c-663cf3f9c8bf\" title=\"Announcements\"  class=\"icon-sakai-announcements\">Announcements</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-resources-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/9adb642c-402f-4178-a4d9-0349cce7034d\" title=\"Resources\"  class=\"icon-sakai-resources\">Resources</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-forums-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/95958334-e0f7-4da9-8b31-be28bd4601b9\" title=\"Forums\"  class=\"icon-sakai-forums\">Forums</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-assignment-grades-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/595cb1e8-42d0-452f-bd9c-72ab224f0d1c\" title=\"Assignments\"  class=\"icon-sakai-assignment-grades\">Assignments</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-gradebook-tool-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/81220c01-e05a-4d5d-9da3-7b1767545cf6\" title=\"Gradebook\"  class=\"icon-sakai-gradebook-tool\">Gradebook</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-mailbox-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/141f8079-d225-4d6c-afb5-29074fd4d618\" title=\"Email Archive\"  class=\"icon-sakai-mailbox\">Email Archive</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-siteinfo-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/f8743e9a-38cc-4b46-9cad-aa83a23c69cc\" title=\"Site Info\"  class=\"icon-sakai-siteinfo\">Site Info</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-sections-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/6ef1fe50-e101-4a43-b8b3-dea61a5518b6\" title=\"Section Info\"  class=\"icon-sakai-sections\">Section Info</a></span>\n" +
            "        </li>\n" +
            "                        <li class=\"icon-sakai-site-roster-item\">\n" +
            "          <span><a href=\"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool-reset/69c2e8e0-61ea-4f7c-b5ef-ff7496473642\" title=\"Roster\"  class=\"icon-sakai-site-roster\">Roster</a></span>\n" +
            "        </li>\n" +
            "                  </ul>\n" +
            "  \n" +
            "<div id=\"pda-footer\">\n" +
            "<a class=\"switch-link\" href=\"https://t-square.gatech.edu/portal/pda/?force.classic=yes\">Switch to Full View</a>\n" +
            "</div>\n" +
            "\n" +
            "</div>";
    public static String assignment = "<form action=\n" +
            "            \"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c\"\n" +
            "            id=\"listAssignmentsForm\" method=\"post\" name=\"listAssignmentsForm\">\n" +
            "                <input name=\"source\" type=\"hidden\" value=\"0\">\n" +
            "                <table border=\"0\" cellspacing=\"0\" class=\n" +
            "                \"listHier lines nolines\" summary=\n" +
            "                \"List of assignments. Column headers are also links which can be used to sort the table by that column. Column 1: Indicates if the assignment has attachments. Column 2: assignment title and links to edit, duplicate or grade(if allowed). Column 3: status. Column 4: opening date. Column 5: due date. The rest of the columns may or may not be present. Column 6: may have the number submitted and graded. Column 7: may have checkboxes to select and remove the assignment.\">\n" +
            "                <tr>\n" +
            "                        <th class=\"attach\" id=\"attachments\">&nbsp;</th>\n" +
            "                        <th id=\"title\"><a href=\"#\" onclick=\n" +
            "                        \"location='https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c?criteria=title&amp;sakai_action=doSort'; return false;\"\n" +
            "                        title=\"Sort by title\">Assignment title</a></th>\n" +
            "                        <th id=\"For\"><a href=\"#\" onclick=\n" +
            "                        \"location='https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c?criteria=for&amp;sakai_action=doSort'; return false;\"\n" +
            "                        title=\"Sort by audience\">For</a></th>\n" +
            "                        <th id=\"status\"><a href=\"#\" onclick=\n" +
            "                        \"location='https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c?criteria=assignment_status&amp;sakai_action=doSort'; return false;\"\n" +
            "                        title=\"Sort by status\">Status</a></th>\n" +
            "                        <th id=\"openDate\"><a href=\"#\" onclick=\n" +
            "                        \"location='https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c?criteria=opendate&amp;sakai_action=doSort'; return false;\"\n" +
            "                        title=\"Sort by section\">Open</a></th>\n" +
            "                        <th id=\"dueDate\"><a href=\"#\" onclick=\n" +
            "                        \"location='https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c?criteria=duedate&amp;sakai_action=doSort'; return false;\"\n" +
            "                        title=\"Sort by due date\">Due</a></th>\n" +
            "                    </tr>\n" +
            "                    <tr>\n" +
            "                        <td class=\"attach\" headers=\"attachments\"><img alt=\n" +
            "                        \"Attachments\" border=\"0\" height=\"11\" id=\"attachment1\"\n" +
            "                        src=\"/library/image/sakai/attachments.gif\" width=\n" +
            "                        \"13\"></td>\n" +
            "                        <td headers=\"title\">\n" +
            "                            <h4><a href=\n" +
            "                            \"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c?assignmentReference=/assignment/a/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/196500bb-7895-4975-ae24-7bee175f054b&amp;sakai_action=doView_submission\">\n" +
            "                            Client Elicitation Interview Assignment</a></h4>\n" +
            "                        </td>\n" +
            "                        <td style=\"padding-bottom:0\">site</td>\n" +
            "                        <td headers=\"status\">Not Started</td>\n" +
            "                        <td headers=\"openDate\">Jan 27, 2016 9:00 am</td>\n" +
            "                        <td headers=\"dueDate\">Jan 31, 2016 11:55 pm</td>\n" +
            "                    </tr>\n" +
            "                    <tr>\n" +
            "                        <td class=\"attach\" headers=\"attachments\">&nbsp;</td>\n" +
            "                        <td headers=\"title\">b\n" +
            "                            <h4><a href=\n" +
            "                            \"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c?assignmentReference=/assignment/a/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/a1f7a1b5-f42b-416e-b3fe-529956496c4a&amp;sakai_action=doView_submission\">\n" +
            "                            Initial Client Contact Email</a></h4>\n" +
            "                        </td>\n" +
            "                        <td style=\"padding-bottom:0\">site</td>\n" +
            "                        <td headers=\"status\">Not Started</td>\n" +
            "                        <td headers=\"openDate\">Jan 20, 2016 9:00 am</td>\n" +
            "                        <td headers=\"dueDate\">Jan 24, 2016 11:55 pm</td>\n" +
            "                    </tr>\n" +
            "                    <tr>\n" +
            "                        <td class=\"attach\" headers=\"attachments\"><img alt=\n" +
            "                        \"Attachments\" border=\"0\" height=\"11\" id=\"attachment3\"\n" +
            "                        src=\"/library/image/sakai/attachments.gif\" width=\n" +
            "                        \"13\"></td>\n" +
            "                        <td headers=\"title\">\n" +
            "                            <h4><a href=\n" +
            "                            \"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c?assignmentReference=/assignment/a/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/48862254-5cf4-4b63-a2ef-fc68ed7ececa&amp;sakai_action=doView_submission\">\n" +
            "                            Communication Objective Worksheet\n" +
            "                            (In-cl...</a></h4>\n" +
            "                        </td>\n" +
            "                        <td style=\"padding-bottom:0\">site</td>\n" +
            "                        <td headers=\"status\">Not Started</td>\n" +
            "                        <td headers=\"openDate\">Jan 20, 2016 9:00 am</td>\n" +
            "                        <td headers=\"dueDate\">Jan 22, 2016 9:00 am</td>\n" +
            "                    </tr>\n" +
            "                    <tr>\n" +
            "                        <td class=\"attach\" headers=\"attachments\"><img alt=\n" +
            "                        \"Attachments\" border=\"0\" height=\"11\" id=\"attachment4\"\n" +
            "                        src=\"/library/image/sakai/attachments.gif\" width=\n" +
            "                        \"13\"></td>\n" +
            "                        <td headers=\"title\">\n" +
            "                            <h4><a href=\n" +
            "                            \"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c?assignmentReference=/assignment/a/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/0399976f-c3db-4576-9cda-dde3bd0bd419&amp;sakai_action=doView_submission\">\n" +
            "                            Project Bid Email</a></h4>\n" +
            "                        </td>\n" +
            "                        <td style=\"padding-bottom:0\">site</td>\n" +
            "                        <td headers=\"status\">Not Started</td>\n" +
            "                        <td headers=\"openDate\">Jan 13, 2016 9:00 am</td>\n" +
            "                        <td headers=\"dueDate\">Jan 17, 2016 11:55 pm</td>\n" +
            "                    </tr>\n" +
            "                    <tr>\n" +
            "                        <td class=\"attach\" headers=\"attachments\"><img alt=\n" +
            "                        \"Attachments\" border=\"0\" height=\"11\" id=\"attachment5\"\n" +
            "                        src=\"/library/image/sakai/attachments.gif\" width=\n" +
            "                        \"13\"></td>\n" +
            "                        <td headers=\"title\">\n" +
            "                            <h4><a href=\n" +
            "                            \"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/595cb1e8-42d0-452f-bd9c-72ab224f0d1c?submissionId=/assignment/s/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/82056375-a948-4de6-8a0e-4990d15cdfcf/c8e3a3d4-4f97-486b-8069-72919b31aa77&amp;sakai_action=doView_grade\">\n" +
            "                            Self Assessment for Collaboration</a></h4>\n" +
            "                        </td>\n" +
            "                        <td style=\"padding-bottom:0\">site</td>\n" +
            "                        <td headers=\"status\">Submitted Jan 14, 2016 10:12\n" +
            "                        am</td>\n" +
            "                        <td headers=\"openDate\">Jan 13, 2016 9:00 am</td>\n" +
            "                        <td headers=\"dueDate\">Jan 15, 2016 11:55 pm</td>\n" +
            "                    </tr>\n" +
            "                </table>\n" +
            "            </form>";
    public static String grade = "<div class=\"portletBody\">\n" +
            "    <form action=\n" +
            "    \"https://t-square.gatech.edu/portal/pda/gtc-3537-5b2f-5e5c-b4dd-d13bbedaf426/tool/81220c01-e05a-4d5d-9da3-7b1767545cf6/studentView\"\n" +
            "    enctype=\"application/x-www-form-urlencoded\" id=\"gbForm\" method=\n" +
            "    \"post\" name=\"gbForm\">\n" +
            "        <table width=\"99%\">\n" +
            "            <tbody>\n" +
            "                <tr>\n" +
            "                    <td class=\"bogus\">\n" +
            "                        <h2>Grade Report for Schwahn, Stephen\n" +
            "                        James</h2>\n" +
            "                    </td>\n" +
            "                    <td class=\"right\"></td>\n" +
            "                </tr>\n" +
            "            </tbody>\n" +
            "        </table>\n" +
            "        <table cellpadding=\"0\" cellspacing=\"0\" class=\"itemSummary\">\n" +
            "            <tbody>\n" +
            "                <tr>\n" +
            "                    <td class=\"itemName\">Course Grade</td>\n" +
            "                    <td>Not yet available</td>\n" +
            "                </tr>\n" +
            "            </tbody>\n" +
            "        </table>\n" +
            "        <h4>Gradebook Items</h4>\n" +
            "        <script type=\"text/javascript\">\n" +
            "        //<![CDATA[\n" +
            "        showHideDiv('_id_0', '/sakai-gradebook-tool', 'Expanded', 'Collapsed', 'Hide items', 'View items');\n" +
            "        //]]>\n" +
            "        </script>\n" +
            "        <table cellpadding=\"0\" cellspacing=\"0\" class=\n" +
            "        \"listHier wideTable lines\">\n" +
            "            <thead>\n" +
            "                <tr>\n" +
            "                    <th class=\"left\" scope=\"col\">\n" +
            "                        <a class=\"notCurrentSort\" href=\"#\" id=\n" +
            "                        \"gbForm:_idJsp22:_idJsp24\" onclick=\n" +
            "                        \"return oamSubmitForm('gbForm','gbForm:_idJsp22:_idJsp24');\">Title</a>\n" +
            "                    </th>\n" +
            "                    <th class=\"center\" scope=\"col\"><a class=\n" +
            "                    \"currentSort\" href=\"#\" id=\n" +
            "                    \"gbForm:_idJsp22:_idJsp29\" onclick=\n" +
            "                    \"return oamSubmitForm('gbForm','gbForm:_idJsp22:_idJsp29');\">\n" +
            "                    Due Date&#160;<img src=\n" +
            "                    \"/sakai-gradebook-tool/images/sortascending.gif\"></a></th>\n" +
            "                    <th class=\"center\" scope=\"col\"><a class=\n" +
            "                    \"notCurrentSort\" href=\"#\" id=\n" +
            "                    \"gbForm:_idJsp22:_idJsp34\" onclick=\n" +
            "                    \"return oamSubmitForm('gbForm','gbForm:_idJsp22:_idJsp34');\">\n" +
            "                    Grade*</a></th>\n" +
            "                    <th class=\"center comments\" scope=\"col\">\n" +
            "                    Comments</th>\n" +
            "                    <th class=\"bogus\" scope=\"col\"></th>\n" +
            "                </tr>\n" +
            "            </thead>\n" +
            "            <tbody>\n" +
            "                <tr class=\"external\" id=\"_id_0__hide_division_\">\n" +
            "                    <td class=\"left\">Client Project Requirements</td>\n" +
            "                    <td class=\"center\">Feb 14, 2016</td>\n" +
            "                    <td class=\"center\">-</td>\n" +
            "                    <td class=\"center\"></td>\n" +
            "                    <td class=\"external\">from Assignments</td>\n" +
            "                </tr>\n" +
            "            </tbody>\n" +
            "        </table>\n" +
            "        <table cellpadding=\"0\" cellspacing=\"0\" class=\n" +
            "        \"instruction gbSection\">\n" +
            "            <tbody>\n" +
            "                <tr>\n" +
            "                    <td>Legend:</td>\n" +
            "                </tr>\n" +
            "                <tr>\n" +
            "                    <td>*Grades in parentheses () are not included in\n" +
            "                    the course grade calculation.</td>\n" +
            "                </tr>\n" +
            "            </tbody>\n" +
            "        </table><input name=\"gbForm_SUBMIT\" type=\"hidden\" value=\n" +
            "        \"1\"><input name=\"gbForm:_link_hidden_\" type=\n" +
            "        \"hidden\"><input name=\"gbForm:_idcl\" type=\"hidden\">\n" +
            "        \n" +
            "    </form>\n" +
            "</div>";
}
