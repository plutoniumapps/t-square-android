package com.plutoniumapps.t_square.Model;

import org.acra.ACRA;
import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import okhttp3.Call;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.exceptions.Exceptions;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

public final class TsquareModel {

    private Observable<List<Course>> _list;

    final String user_agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 8_0 like Mac OS X) AppleWebKit/600.1.3 (KHTML, like Gecko) Version/8.0 Mobile/12A4345d Safari/600.1.4";

    private final int LIST_OF_CLASSES = 0;
    private final int NAME_OF_CLASS= 1;
    private final int CLASS_ANNOUNCEMENTS = 2;
    private final int CLASS_ASSIGNMENTS= 3;
    private final int CLASS_GRADES= 4;
    private final int CLASS_SYLLABUS= 5;
    private final int SITE_INFO = 6;

    private final String[] _resources = {"pda-portlet-site-menu", "currentSiteLink",
            "icon-sakai-announcements","icon-sakai-assignment-grades","icon-sakai-gradebook-tool",
            "icon-sakai-syllabus", "icon-sakai-iframe-site"};
    private final String _home = "https://t-square.gatech.edu/portal/pda/?force.login=yes";


    public OkHttpClient _client;
    private String username;
    private String password;
    private boolean _haveSession;

    public TsquareModel(){
        _client = buildClient();
    }

    public TsquareModel(String u, String p){
        _client = buildClient();
        username = u;
        password = p;
        _haveSession = false;
    }

    public void printCookie(){
        List<Cookie> list = _client.cookieJar().loadForRequest(new HttpUrl.Builder().scheme("https").host("t-square.gatech.edu").build());
        for (Cookie c : list){
            print(c.toString());
        }
    }
    private OkHttpClient buildClient(){
        _haveSession = false;
        OkHttpClient c = new OkHttpClient.Builder().
            cookieJar(new CookieJar() {
                private final HashMap<String, Cookie> cookieStore = new HashMap<String, Cookie>();

                @Override
                public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
                    for (Cookie c : cookies) {
                        cookieStore.put(c.name(), c);
                    }
                }

                @Override
                public List<Cookie> loadForRequest(HttpUrl url) {
                    return cookieStore != null ? new ArrayList<Cookie>(cookieStore.values()) : new ArrayList<Cookie>();
                }
            })
            .readTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(6, TimeUnit.SECONDS)
            .build();
            return c;
    }

    //public interfaces

    public Observable<Boolean> verifyLogin(String username, String password){
        this.password = password;
        this.username = username;
        return tsquareDocument(_home).map(response -> true);
    }

    public Observable<List<Course>> courseList(boolean cached){
        if (_list == null) {
            return _list = networkLoad().doOnError(err -> {
                _list = null;
            }).cache();
        }
        //If we don't want a cached response, call networkLoad() again and store the response
        if(!cached){
            _list = networkLoad().doOnError(err -> {
                _list = null;
            }).cache();
        }
        return _list;

    }
    public void logout(){
        _list = null;
        _client = buildClient();
        username = "";
        password = "";
    }
    public Observable<Course> getCourseByName(String name){
        return courseList(true).flatMap(list -> Observable.from(list.toArray(new Course[list.size()])))
                .filter(course -> course.className().equals(name)).first();
    }

    //This is only for the viewer activity
    public Response getPageForWebView(String url) throws IOException{
        //TODO:first make sure we're logged in
        Request request = buildGetRequest(url);
        final Call call = _client.newCall(request);

        return call.execute();
    }

    public Observable<Response> downloadResource(String url){
        return tsquareDocument(_home).flatMap(loggedIn -> {
            return request(buildGetRequest(url));
        });
    }

    public Observable<Announcement> resolveAnnouncement(Announcement a){
        return tsquareDocument(a._messageurl).map(document -> {
            Elements div = document.select("div.portletBody");
            div.select("form[name=formII]").remove();
            div.select("h4:contains(Message)").remove();
            div.select("h3:contains(Announcement)").remove();
            div.select("table.itemSummary").remove();
            div.select(".actionToolBar").remove();
            a.setMessage(div.html());
            return a;
        });
    }

    public Observable<Assignment> resolveAssignmentHtml(Assignment a){
        return tsquareDocument(a._detailUrl).map(doc -> {
            Elements display = doc.select("div.portletBody");
            display.select("form").remove();

            a.setBodyHtml(display.html());

            return a;
        });
    }

    //private observable building blocks

    private Request buildGetRequest(String url) {
        return new Request.Builder()
                .url(url)
                .header("User-Agent",user_agent)
                .build();
    }

    private Observable<Response> request(Request request) {
        return Observable.create(subj -> {
            final Call call = _client.newCall(request);

            subj.add(Subscriptions.create(call::cancel));
            Response r;
            try {
                r = call.execute();
            } catch (IOException i) {
                print(request.toString());
                print(i.getMessage());
                subj.onError(i);
                return;
            }

            if (!r.isSuccessful()) {
                if (r.code() >= 500){
                    subj.onError(new IOException("Server Level Error: " + r.code()));
                } else {
                    subj.onError(new IOException("Didn't get a OK/200: " + r.code()));
                }

                return;
            }
            subj.onNext(r);
            subj.onCompleted();
        });
    }

    private Observable<Document> getDocument(String url){
        String clean_url = url.replace("tool-reset","tool");
        Request r = buildGetRequest(clean_url);
        return request(r).retry(5).map(response -> {
            String body_text = "";
            try {
                body_text = response.body().string();
            } catch (IOException i) {
                throw Exceptions.propagate(i);
            } finally {
                response.body().close();
            }
            return Jsoup.parse(body_text);
        });
    }

    private Observable<Document> postLogin(String lt){
        print("Had to login");
        if(password == null ||  password.isEmpty())
            throw Exceptions.propagate(new InvalidCredentialsException(("Empty Login")));
        HttpUrl login_point = HttpUrl.parse("https://login.gatech.edu/cas/login");
        RequestBody formBody = new FormBody.Builder()
                .add("warn", "true")
                .add("lt", lt)
                .add("execution", "e1s1")
                .add("_eventId", "submit")
                .add("submit", "LOGIN")
                .add("username", username)
                .add("password", password)
                .build();
        Request login_post = new Request.Builder()
                .url(login_point.newBuilder()
                        .addEncodedQueryParameter("service", "https%3A%2F%2Ft-square.gatech.edu%2Fsakai-login-tool%2Fcontainer")
                        .build())
                .addHeader("Referer", "https://login.gatech.edu/cas/login?service=https%3A%2F%2Ft-square.gatech.edu%2Fsakai-login-tool%2Fcontainer")
                .header("User-Agent", user_agent)
                .post(formBody)
                .build();
        return request(login_post).flatMap(post -> {
            Document postPage;
            try {
                //Android studio bug ...
                ResponseBody b = post.body();
                String s = b.string();
                postPage = Jsoup.parse(s);
            } catch (IOException i) {
                //if for some reason the body has no text
                throw Exceptions.propagate(i);
            } finally {
                post.body().close();
            }
            if (postPage.select("title").text().equals("GT | GT Login")) {
                //Did not login successfully
                throw Exceptions.propagate(new InvalidCredentialsException("Login Error"));
            }
            return Observable.just(postPage);
        });
    }

    private boolean loggedIn(Document d) {
        return d.select("a[href=https://t-square.gatech.edu/portal/pda/?force.logout=yes]").text().contains("Log Out");

    }

    //tsquareDocument is an abstractino of getDocument that handles the session associated with
    //tsquare's authenticatino system
    //This method can only be used to GET pages on the https://t-square.gatech.edu/portal/pda/ domain
    //No posting anywhere and no GET requests to any other domain.
    private Observable<Document> tsquareDocument(String url){
        if (!_haveSession){
            //we don't even have a JSESSIONID, need to go to homepage and then post login
            return getDocument("https://t-square.gatech.edu/portal/pda/?force.login=yes").flatMap(doc -> {
                //we better have a session now b/c we just requested a /pda/ webpage
                _haveSession = true;


                //for some reason we're logged in. this should never be the case if we're setting
                //_haveSession correctly
                if(loggedIn(doc)){
                    print("LOGIC ERROR IN TSQUAREDOCUMENT");
                    return Observable.just(doc);
                } else {
                    //already have a session, now login

                    //Even tho we requested a /pda/ page, we're not logged in so we're actually at
                    //the login page, so we should be able to find the lt string

                    String lt = doc.select("input").select("[name=lt]").attr("value");
                    return postLogin(lt).flatMap(postLoginResponseDoc -> {
                        if (!loggedIn(postLoginResponseDoc)){
                            ACRA.getErrorReporter().handleSilentException(new IllegalStateException("postLogin() returned but we're not logged in ..."));
                            throw Exceptions.propagate(new IllegalStateException("postLogin() returned but we're not logged in ..."));
                        }
                        return getDocument(url);
                    });
                }
            });
        } else {
            //we have a session, but it might not be valid. Make the original request but before
            //returning check to make sure we're logged in.

            //4 requests here:
            // 1. Original page -> we find we're not logged in
            // 2. GT login page
            // 3. post the login
            // 4. the original page again
            return getDocument(url).flatMap(doc -> {
                if (!loggedIn(doc)) {
                    print("doc was not a loggedin doc");
                    print(doc.toString());
                    print("offending URL: " + url);
                    printCookie();
                    return getDocument("https://t-square.gatech.edu/portal/pda/?force.login=yes").flatMap(loginDoc -> {
                        String lt;
                        lt = loginDoc.select("input").select("[name=lt]").attr("value");
                        return postLogin(lt).flatMap(postLoginResponseDoc -> {
                            if (!loggedIn(postLoginResponseDoc)) {
                                ACRA.getErrorReporter().handleSilentException(new IllegalStateException("postLogin() returned but we're not logged in ..."));
                                throw Exceptions.propagate(new IllegalStateException("postLogin() returned but we're not logged in ..."));
                            }
                            return getDocument(url);
                        });
                    });
                }
                return Observable.just(doc);
            });
        }
    }

    private Observable<List<Course>> networkLoad(){
        return findClassLinks().flatMap(s ->
                Observable.just(s)
                        .subscribeOn(Schedulers.io())
                        .flatMap(sc ->
                                resolveCourse(sc)))
                .filter(c -> c != null)
                .toList();
    }

    private Observable<String> findClassLinks(){
        return tsquareDocument(_home).flatMap(d -> {
            Element list = d.getElementById(_resources[LIST_OF_CLASSES]);
            Elements items = list.select("a[href~=^https?://t-square.gatech.edu/portal/pda/gtc-(\\w|-)+$]");
            ArrayList<String> links = new ArrayList<String>();
            for (Element item : items) {
                links.add(item.attr("href"));
            }
            String[] i = links.toArray(new String[links.size()]);
            return Observable.from(i);
        });

    }

    private Observable<Course> resolveCourse(String class_link) {
        return tsquareDocument(class_link).flatMap(d -> {
            /*
            if (!d.select("a[title=Statistics]").isEmpty()) {
                //There can be classes that user is a TA of that completely break the parsing logic
                //This block will attempt to filter out those courses before they are parsed
                return Observable.just(null);
            }
            */

            Elements name_item = d.select("a[href=" + class_link + "]");

            Elements assignments_item = d.select("." + _resources[CLASS_ASSIGNMENTS]);
            Elements announcements_item = d.select("." + _resources[CLASS_ANNOUNCEMENTS]);
            Elements grades_item = d.select("." + _resources[CLASS_GRADES]);
            //From the iOS source, I never had an example of it being called Markbook but its in the iphone version
            if (grades_item.isEmpty()) {
                grades_item = d.select("a[title=Markbook");
            }
            Elements syllabus_item = d.select("." + _resources[CLASS_SYLLABUS]);
            Elements siteinfo_item = d.select("." + _resources[SITE_INFO]);
            Elements resources_item = d.select("a[title=Resources]");


            String assignments_link = "";
            String announcements_link = "";
            String grades_link = "";
            String syllabus_link = "";
            String siteinfo_link = "";
            String resources_link = "";

            if (assignments_item.size() == 1)
                assignments_link = assignments_item.first().attr("href");
            if (grades_item.size() == 1)
                grades_link = grades_item.first().attr("href");
            if (siteinfo_item.size() == 1)
                siteinfo_link = siteinfo_item.select("a").attr("href");
            if (syllabus_item.size() == 1)
                syllabus_link = syllabus_item.first().attr("href");
            if (announcements_item.size() == 1)
                announcements_link = announcements_item.first().attr("href");
            if (resources_item.size() == 1)
                resources_link = resources_item.first().attr("href");


            String name = name_item.first().text();

            Course c = new Course(name);

            print("Resolving course: " + name);

            Observable<List<Assignment>> a = resolveAssignments(assignments_link);
            Observable<Gradebook> g = resolveGradeBook(grades_link);
            Observable<String> e = resolveEnglishName(siteinfo_link);
            Observable<List<Announcement>> an = resolveAnnouncements(announcements_link);
            Observable<List<Syllabus>> s = resolveSyllabus(syllabus_link);
            Observable<Folder> f = resolveResourceRoot(resources_link);

            return Observable.zip(a.subscribeOn(Schedulers.io()), g.subscribeOn(Schedulers.io()),
                    e.subscribeOn(Schedulers.io()), an.subscribeOn(Schedulers.io()),
                    s.subscribeOn(Schedulers.io()), f.subscribeOn(Schedulers.io()),
                    (assignment_list, gradebook, english_name, announcements, syll, fold) -> {
                        c.setAssignments(assignment_list);
                        c.setGradebook(gradebook);
                        c.setEnglishName(english_name);
                        c.setAnnouncements(announcements);
                        c.setResourceRoot(fold, syll);
                        print("done zipping " + name);
                        return c;
                    });


        });
    }

    private Observable<List<Syllabus>> resolveSyllabus(String url){
        if (url.isEmpty())
            return Observable.just(new ArrayList<>());
        url = url + "/main";
        return tsquareDocument(url).map(d -> {
            List<Syllabus> list = new ArrayList<Syllabus>();

            Elements t = d.select("table.indnt1").select("td a");
            Elements div = d.select("table[summary^=This table presents]").select("div.textPanel");
            if (t.isEmpty() && !div.isEmpty()) {
                Syllabus pure_html = new Syllabus("Syllabus", "this", ".text");
                pure_html.setHtml(div.html());
                list.add(pure_html);
                return list;
            } else if (t.isEmpty()) {
                return new ArrayList<Syllabus>();
            }

            for (Element a : t) {
                String resource_name = a.text();
                String resource_url = a.attr("href");
                String resource_type;
                int index = resource_url.lastIndexOf(".");

                if (index > -1)
                    resource_type = resource_url.substring(resource_url.lastIndexOf("."));
                else
                    resource_type = "internal";
                if (!(resource_name.isEmpty() || resource_url.isEmpty()))
                    list.add(new Syllabus(resource_name, "https://t-square.gatech.edu" + resource_url, resource_type));
            }
            return list;
        });
    }

    private Observable<List<Announcement>> resolveAnnouncements(String url){
        if(url.isEmpty())
            return Observable.just(new ArrayList<>());
        return tsquareDocument(url + "?selectPageSize=100&eventSubmit_doChange_pagesize=changepagesize").map(d -> {
            List<Announcement> announcements = new ArrayList();

            Elements t = d.select("table tr:gt(0)");

            if (t.isEmpty()){
                return announcements;
            }

            for (Element r : t) {
                String date = r.select("td[headers=date]").text();
                String subject = r.select("td[headers=subject] a").text();
                String u = r.select("td[headers=subject] a").attr("href");
                String author = r.select("td[headers=author]").text();

                announcements.add(new Announcement(date, subject, author, u));
            }
            return announcements;
        });
    }

    private Observable<String> resolveEnglishName(String siteInfoURL){
        if (siteInfoURL.isEmpty())
            return Observable.just("");
        return tsquareDocument(siteInfoURL).map(d -> {
            Elements s = d.select("div.siteDescription");
            String site_text = "";
            if (!s.isEmpty()) {
                site_text = s.text().replace("\n", " ");
                int word_count = site_text.split(" ").length;

                //Filtering logic below taken from iphone app
                if (word_count > 10 || site_text.contains("--NO TITLE--") || site_text.toLowerCase().contains("welcome"))
                    return "";
            }
            return site_text;
        });
    }

    private Observable<List<Assignment>> resolveAssignments(String assignmentsURL){
        if(assignmentsURL.isEmpty())
            return Observable.just(new ArrayList<>());
        return tsquareDocument(assignmentsURL).map(d -> {
            DateFormat format = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
            List<Assignment> assignments = new ArrayList<Assignment>();
            if (d.select("p").select("[class=instruction]").text().startsWith("There are currently no assignments at this location.")) {
                return assignments;
            }
            Elements tr = d.select("tr");
            if (tr.size() < 2)
                throw new IllegalStateException();

            for (int i = 1; i < tr.size(); i++) {

                Element a = tr.get(i).select("[headers=title]").select("h4").select("a").first();

                String title = a.text();
                String url = a.attr("href");
                String unknown = tr.get(i).select("[style=padding-bottom:0]").text();
                String status = tr.get(i).select("[headers=status]").text();
                String assigned_date_string = tr.get(i).select("[headers=openDate]").text();
                String due_date_string = tr.get(i).select("[headers=dueDate]").text();
                assignments.add(new Assignment(title, unknown, status, assigned_date_string, due_date_string, url));
            }
            return assignments;
        });
    }

    private Observable<Gradebook> resolveGradeBook(String gradebookURL){
        if (gradebookURL.isEmpty())
            return Observable.just(new Gradebook());
        gradebookURL = gradebookURL + "/studentView.jsf";
        return tsquareDocument(gradebookURL).map(d -> {


            String course_grade;

            if (d.select(".itemName + td").select("span").isEmpty()) {
                course_grade = d.select(".itemName + td").text();
            } else {
                course_grade = d.select(".itemName + td").select("span[id=gbForm:cumScore]").text();
            }

            int name_index = -1, grade_index = -1, weight_index = -1, comment_index = -1, due_index = -1;


            Elements table_headers = d.select("thead").select("tr th");
            for (int i = 0; i < table_headers.size(); i++) {
                String text = table_headers.get(i).text().toLowerCase();
                if (text.contains("title"))
                    name_index = i;
                else if (text.contains("grade"))
                    grade_index = i;
                else if (text.contains("weight"))
                    weight_index = i;
                else if (text.contains("comment"))
                    comment_index = i;
                else if (text.contains("due date"))
                    due_index = i;
            }

            //All the rows in tbody
            LinkedHashMap<GradeCategory, List<Grade>> grade_matrix
                    = new LinkedHashMap<GradeCategory, List<Grade>>();
            Elements rows = d.select("table.lines").select("tbody tr");


            if (rows.isEmpty()) {
                return new Gradebook(course_grade, grade_matrix);
            }

            int column_count;
            try {
                column_count = rows.get(0).select("td").size();
            } catch (IndexOutOfBoundsException i) {
                ACRA.getErrorReporter().handleSilentException(i);
                return new Gradebook(course_grade, grade_matrix);
            }


            ArrayList<Grade> list = new ArrayList<Grade>();
            GradeCategory f = new GradeCategory("", "", "");
            for (Element e : rows) {
                //New category
                Elements sp = e.select("td.left > span.categoryHeading");
                if (!sp.isEmpty()) {
                    //Check for anonymous grades before the first category
                    if (f.anonCategory() && !list.isEmpty()) {
                        grade_matrix.put(f, list);
                    } else if (!list.isEmpty()) {
                        grade_matrix.put(f, list);
                    }

                    String cat_name = sp.text();
                    String cat_grade = "", cat_weight = "";
                    if (grade_index > -1) cat_grade = e.select("td").get(grade_index).text();
                    if (weight_index > -1) cat_weight = e.select("td").get(weight_index).text();
                    f = new GradeCategory(cat_name, cat_grade, cat_weight);
                    list = new ArrayList<Grade>();
                } else {
                    String grade_name = "", due_date = "", grade = "", comments = "";
                    if (name_index > -1) grade_name = e.select("td").get(name_index).text();
                    if (due_index > -1) due_date = e.select("td").get(due_index).text();
                    if (grade_index > -1) grade = e.select("td").get(grade_index).text();
                    if (comment_index > -1) comments = e.select("td").get(comment_index).text();

                    list.add(new Grade(grade_name, due_date, grade, comments));
                }

                //Check for last category
                if (!list.isEmpty())
                    grade_matrix.put(f, list);

            }
            return new Gradebook(course_grade, grade_matrix);
        });
    }

    public Observable<Folder> resolveFolder(Folder f){
        return tsquareDocument(f.getUrl()).map(d -> {

            //make sure we don't have stale date or duplicates
            f.clear();

            Elements rows = d.select("tr h4");
            for (Element r : rows) {
                Elements a = r.select("a[onclick]");
                if (!a.isEmpty()) {
                    Element link;

                    if(a.size() == 1){
                        link = a.first();
                    } else {
                        link = a.get(1);
                    }

                    String on_click = link.attr("onclick");

                    String id = on_click.substring(on_click.lastIndexOf("collectionId').value='") + 22);
                    id = id.substring(0, id.indexOf("'", 0));

                    String nav = on_click.substring(on_click.lastIndexOf("navRoot').value='") + 17);
                    nav = nav.substring(0, nav.indexOf("'", 0));


                    String name = link.text();

                    f.append(new Folder(name, f.path, id, nav, f));


                    continue;
                }
                a = r.select("a");
                if (!a.isEmpty()) {
                    Element link;
                    if(a.size() == 1){
                        link = a.get(0);
                    } else {
                        link = a.get(1);
                    }

                    String path = link.attr("href");
                    String name = link.text();

                    String title = link.attr("title");
                    if (title.equals("Web link (URL)") || title.equals("URL")) {
                        //external link resource
                        f.append(new Resource(name, path, "www"));
                    } else {
                        String extension;
                        int index = path.lastIndexOf(".");
                        if (index < 1) {
                            extension = "Error";
                        } else {
                            extension = path.substring(index);
                        }
                        f.append(new Resource(name, path, extension));
                    }
                }
            }

            return f;
        });
    }

    private Observable<Folder> resolveResourceRoot(String url){
        if (url.isEmpty())
            return Observable.just(new Folder());
        return tsquareDocument(url).map(d -> {
            Elements rows = d.select("tr h4");

            Folder root = new Folder("Root", url, "", "", null);
            for (Element r : rows) {
                Elements a = r.select("a[onclick]");
                if (!a.isEmpty()) {
                    Element link;

                    if(a.size() == 1){
                        link = a.first();
                    } else {
                        link = a.get(1);
                    }

                    String on_click = link.attr("onclick");

                    String id = on_click.substring(on_click.lastIndexOf("collectionId').value='") + 22);
                    id = id.substring(0, id.indexOf("'", 0));

                    String nav = on_click.substring(on_click.lastIndexOf("navRoot').value='") + 17);
                    nav = nav.substring(0, nav.indexOf("'", 0));


                    String name = link.text();

                    root.append(new Folder(name, url, id, nav, root));


                    continue;
                }
                a = r.select("a");
                if (!a.isEmpty()) {
                    Element link;
                    if(a.size() == 1){
                        link = a.get(0);
                    } else {
                        link = a.get(1);
                    }

                    String path = link.attr("href");
                    String name = link.text();

                    String title = link.attr("title");
                    if (title.equals("Web link (URL)") || title.equals("URL")) {
                        //external link resource
                        root.append(new Resource(name, path, "www"));
                    } else {
                        String extension;
                        int index = path.lastIndexOf(".");
                        if (index < 1) {
                            extension = "Error";
                        } else {
                            extension = path.substring(index);
                        }
                        root.append(new Resource(name, path, extension));
                    }
                }
            }
            return root;
            
        });
    }

    void print(String s) {
        System.out.println(s);
    }

    public static void main(String[] args) {
        Map<String,String> env = System.getenv();

        String u = env.get("TUSERNAME");
        String p = env.get("TPASS");

        TsquareModel m = new TsquareModel();

        m.verifyLogin(u,p);

        m.courseList(false).toBlocking().subscribe(cr -> {
            for (Course c : cr){
                System.out.println(c.toString());
            }

        }, e -> e.printStackTrace());
    }


}
