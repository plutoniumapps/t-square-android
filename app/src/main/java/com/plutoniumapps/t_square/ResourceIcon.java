package com.plutoniumapps.t_square;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.widget.ImageView;

public class ResourceIcon extends AppCompatImageView {

    public ResourceIcon(final Context context) {
        super(context);
    }

    public ResourceIcon(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public ResourceIcon(final Context context, final AttributeSet attrs,
                        final int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int width, int height) {
        super.onMeasure(width, height);
        int measuredWidth = getMeasuredWidth();
        setMeasuredDimension(measuredWidth,measuredWidth);
    }

}