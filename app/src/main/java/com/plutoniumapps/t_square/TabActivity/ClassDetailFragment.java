package com.plutoniumapps.t_square.TabActivity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.plutoniumapps.t_square.Adapters.AnnouncementsAdapter;
import com.plutoniumapps.t_square.Adapters.AssignmentsAdapter;
import com.plutoniumapps.t_square.Adapters.GradesAdapter;
import com.plutoniumapps.t_square.Adapters.ResourceAdapter;
import com.plutoniumapps.t_square.Adapters.UpdateableAdapter;
import com.plutoniumapps.t_square.DividerItemDecoration;
import com.plutoniumapps.t_square.Model.Course;
import com.plutoniumapps.t_square.R;
import com.plutoniumapps.t_square.Tsquare;

import org.acra.ACRA;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ClassDetailFragment extends Fragment {

    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String ARG_CNAME = "ARG_CNAME";
    public static final int PAGE_ANNOUNCEMENTS = 3;
    public static final int PAGE_ASSIGNMENTS = 2;
    public static final int PAGE_GRADES = 1;
    public static final int PAGE_RESOURCES = 4;

    private RecyclerView.Adapter mAdapter;


    private int mPage;
    private String mCourseName;
    public CompositeSubscription mSub;
    private Course mCourse;
    private Dialog mLoadingDialog;

    public static ClassDetailFragment newInstance(int page, String cn) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        args.putString(ARG_CNAME, cn);

        ClassDetailFragment fragment = new ClassDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
        mCourseName = getArguments().getString(ARG_CNAME);

        mLoadingDialog = new Dialog(getActivity());
        mLoadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mLoadingDialog.setContentView(R.layout.dialog_progress);
        mLoadingDialog.setCancelable(false);
    }

    @Bind(R.id.single_class_recycler) RecyclerView mRecyclerView;
    @Bind(R.id.empty_text) TextView mEmptyText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_class_detail, container, false);
        ButterKnife.bind(this, view);
        Context c = container.getContext();
        switch(mPage) {
            case PAGE_ANNOUNCEMENTS:
                mAdapter = new AnnouncementsAdapter(null,this);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(c));
                mRecyclerView.addItemDecoration(new DividerItemDecoration(c));
                break;
            case PAGE_ASSIGNMENTS:
                mAdapter = new AssignmentsAdapter(null,this);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(c));
                mRecyclerView.addItemDecoration(new DividerItemDecoration(c));
                break;
            case PAGE_GRADES:
                mAdapter = new GradesAdapter(null,this);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(c));
                break;
            case PAGE_RESOURCES:
                GridLayoutManager layout = new GridLayoutManager(c, 2);
                mAdapter = new ResourceAdapter(null, this, layout);
                mRecyclerView.setLayoutManager(layout);
                mRecyclerView.setBackgroundResource(R.color.white);
                break;
        }
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        hideLoad();

        mSub = new CompositeSubscription();
        Tsquare t = (Tsquare) getActivity().getApplication();
        mSub.add(t.getModel().getCourseByName(mCourseName).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(success -> {
                    mCourse = success;
                    if (mCourse != null) {
                        ((UpdateableAdapter) mAdapter).updateCourse(mCourse);
                    } else {
                        Toast.makeText(getActivity(), "Couldn't find this class", Toast.LENGTH_LONG).show();
                    }
                }, error -> {
                    error.printStackTrace();
                    ACRA.getErrorReporter().handleSilentException(error);
                    Toast.makeText(getActivity(), "Network Error", Toast.LENGTH_LONG).show();
                    hideLoad();
                }));

    }

    @Override
    public void onPause(){
        super.onPause();
        if(mSub != null)
            mSub.unsubscribe();
        mSub = null;
    }

    public void emptyView(boolean show){
        if (show){
            mRecyclerView.setVisibility(View.GONE);
            mEmptyText.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyText.setVisibility(View.GONE);
        }
    }

    public void showLoad(){
        if(mLoadingDialog != null){
            mLoadingDialog.show();
        }
    }

    public void hideLoad(){
        if(mLoadingDialog != null && mLoadingDialog.isShowing()){
            mLoadingDialog.dismiss();
        }
    }
}
