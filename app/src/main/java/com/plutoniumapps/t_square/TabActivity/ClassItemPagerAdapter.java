package com.plutoniumapps.t_square.TabActivity;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ClassItemPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 4;
    private String tabTitles[] = new String[] {"Grades","Assignments","Announcements","Resources"};
    private Context context;
    private String mCourseName;

    public ClassItemPagerAdapter(FragmentManager fm, Context context, String cn) {
        super(fm);
        this.context = context;
        mCourseName = cn;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return ClassDetailFragment.newInstance(position + 1,mCourseName);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}