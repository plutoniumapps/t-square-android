package com.plutoniumapps.t_square.TabActivity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.plutoniumapps.t_square.ClassListActivity;
import com.plutoniumapps.t_square.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class SingleClassTabActivity extends AppCompatActivity {

    public static final String ANNOUNCEMENT_VIEW_ANNOUNCEMENT = "com.t_square.announcement_view";


    @Bind(R.id.viewpager) ViewPager mViewPager;
    @Bind(R.id.tabs) SmartTabLayout mTabLayout;
    @Bind(R.id.toolbar) Toolbar mToolBar;

    private String mCourseName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_class_tab);
        ButterKnife.bind(this);

        mCourseName = getIntent().getStringExtra(ClassListActivity.CLASS_NAME_EXTRA);
        String clean = getIntent().getStringExtra(ClassListActivity.CLASS_CLEAN_NAME_EXTRA);
        mToolBar.setTitle(clean);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mViewPager.setAdapter(new ClassItemPagerAdapter(getSupportFragmentManager(), this, mCourseName));
        mTabLayout.setViewPager(mViewPager);



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
