package com.plutoniumapps.t_square;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;

import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.IoniconsModule;
import com.joanzapata.iconify.fonts.MaterialCommunityModule;
import com.joanzapata.iconify.fonts.MaterialModule;
import com.plutoniumapps.t_square.Model.TsquareModel;
import com.securepreferences.SecurePreferences;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.annotation.*;
import org.acra.sender.HttpSender;

@ReportsCrashes(
        formUri = "https://cjzeiger.cloudant.com/acra-tsquare/_design/acra-storage/_update/report",
        reportType = HttpSender.Type.JSON,
        httpMethod = HttpSender.Method.POST,
        formUriBasicAuthLogin = "porksomedgdozedigainglay",
        formUriBasicAuthPassword = "59ed5455f77161ce3b8d8c79a8cfa3fffd661422",
        customReportContent = {
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.PACKAGE_NAME,
                ReportField.REPORT_ID,
                ReportField.BUILD,
                ReportField.STACK_TRACE,
                ReportField.CUSTOM_DATA
        }
)
public class Tsquare extends Application {

    private TsquareModel tmodel;
    public static final String KEY_USERNAME = "com.t_square.username";
    public static final String KEY_PASSWORD = "com.t_square.password";

    @Override
    public void onCreate() {
        super.onCreate();
        if(!isACRAService()){
            Iconify.with(new MaterialModule())
                    .with(new MaterialCommunityModule())
                    .with(new IoniconsModule());

            SharedPreferences pfs = new SecurePreferences(this);
            String username = pfs.getString(KEY_USERNAME,"");
            String password = pfs.getString(KEY_PASSWORD,"");
            tmodel = new TsquareModel(username, password);
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

        ACRA.init(this);
    }

    public TsquareModel getModel(){
        return tmodel;
    }

    private boolean isACRAService(){
        final int processId = android.os.Process.myPid();
        final ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);

        String procName = "";
        for (final ActivityManager.RunningAppProcessInfo processInfo : manager.getRunningAppProcesses()){
            if(processInfo.pid == processId){
                procName = processInfo.processName;
            }
        }
        return procName != null && procName.equals(":acra");
    }

}
