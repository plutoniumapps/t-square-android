package com.plutoniumapps.t_square;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;

import okhttp3.Response;

public class TsquareWebClient extends WebViewClient {
    private Tsquare mApp;
    public TsquareWebClient(Tsquare app){
        super();
        mApp = app;

    }
    @SuppressWarnings("deprecation")
    @Override
    public WebResourceResponse shouldInterceptRequest(WebView webView, String URL){
        if (urlShouldBeHandledByWebView(URL)) {
            return super.shouldInterceptRequest(webView, URL);
        }

        return handleRequestViaOkHttp(URL);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public WebResourceResponse shouldInterceptRequest(@NonNull WebView view, @NonNull WebResourceRequest request) {
        if (urlShouldBeHandledByWebView(request.getUrl().toString())) {
            return super.shouldInterceptRequest(view, request);
        }

        return handleRequestViaOkHttp(request.getUrl().toString());
    }

    static boolean urlShouldBeHandledByWebView(@NonNull String url) {
        // file: Resolve requests to local files such as files from cache folder via WebView itself
        return url.startsWith("file:");
    }

    @NonNull
    private WebResourceResponse handleRequestViaOkHttp(@NonNull String url) {
        try {
            // On Android API >= 21 you can get request method and headers
            // As I said, we need to only display "simple" page with resources
            // So it's GET without special headers


            final Response response = mApp.getModel().getPageForWebView(url);
            System.out.println("In the custom client, reponse is : \t " + response.toString() + "content-type is : " + response.header("content-type", "text/plain") + " content encoding is: " + response.header("content-encoding", "utf-8") + " content-disposition: " + response.header("Content-Disposition") );


            //The old webserver returns bad content-type headers, need to override them.
            String content_type = response.header("content-type","text/html");
            if(content_type.startsWith("text/html"))
                content_type = "text/html";
            if(content_type.startsWith("application/pdf")){
                //String filename = downloadResource(response);
                return new WebResourceResponse("application/pdf","",response.body().byteStream());
            }

            return new WebResourceResponse(
                    content_type,//response.header("content-type", "text/plain"), // You can set something other as default content-type
                    response.header("content-encoding", "utf-8"),  // Again, you can set another encoding as default
                    response.body().byteStream()
            );
        } catch (Exception e) {
            return new WebResourceResponse("text/plain","utf-8",new ByteArrayInputStream("Networking error".getBytes())); // return response for bad request
        }
    }

    private String downloadPdf(Response r) throws Exception{
        String fileName = r.header("Content-Disposition","filename=syllabus.pdf");
        fileName = fileName.substring(fileName.indexOf("filename=\"")+10, fileName.lastIndexOf("\""));
        System.out.println("filename: " + fileName);
        FileOutputStream fos = mApp.openFileOutput(fileName, Context.MODE_PRIVATE);
        fos.write(r.body().bytes());
        fos.close();
        return fileName;
    }


}
