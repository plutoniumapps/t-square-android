package com.plutoniumapps.t_square;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.plutoniumapps.t_square.TabActivity.SingleClassTabActivity;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public class ViewerActivity extends AppCompatActivity {

    private String mUrl;
    private CompositeSubscription mSub;

    public static final String VIEWER_NAME = "com.t_square.viewer_name";
    public static final String VIEWER_URL = "com.t_square.viewer_url";
    public static final String VIEWR_DATA = "com.t_square.viewer_data";

    @Bind(R.id.webview) WebView mWebView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewer);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getStringExtra(VIEWER_NAME));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mUrl = getIntent().getStringExtra(VIEWER_URL);
        System.out.println("url: " + mUrl);
        //mWebView.getSettings().setJavaScriptEnabled(true);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.getSettings().setAllowFileAccess(true);
        mWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);

        mWebView.setWebChromeClient(new WebChromeClient());

        if(mUrl.equals("this")){
            mWebView.setWebViewClient(new WebViewClient());
            String html = getIntent().getStringExtra(VIEWR_DATA);
            mWebView.loadData(html,"text/html; charset=utf-8", "utf-8");
        } else {
            mWebView.setWebViewClient(new TsquareWebClient((Tsquare) getApplication()));
            mWebView.loadUrl(mUrl);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home){
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
